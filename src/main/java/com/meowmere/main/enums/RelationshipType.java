package com.meowmere.main.enums;

//W przypadku dodania nowego typu, trzeba tak samo zaktualizować enum na FE!
public enum RelationshipType {
    PARENT,
    CHILD,
    MARRIAGE
}
