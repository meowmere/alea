package com.meowmere.main.dto.statistics;

public class TypeStatisticsDTO {
    public int mainCharactersNum;
    public int sideCharactersNum;
    public int bgCharactersNum;

    public int getMainCharactersNum() {
        return mainCharactersNum;
    }

    public void setMainCharactersNum(int mainCharactersNum) {
        this.mainCharactersNum = mainCharactersNum;
    }

    public int getSideCharactersNum() {
        return sideCharactersNum;
    }

    public void setSideCharactersNum(int sideCharactersNum) {
        this.sideCharactersNum = sideCharactersNum;
    }

    public int getBgCharactersNum() {
        return bgCharactersNum;
    }

    public void setBgCharactersNum(int bgCharactersNum) {
        this.bgCharactersNum = bgCharactersNum;
    }
}
