export interface IRelatedCharacter {
  id: number;
  charName: string;
  charSurname: string;
}
