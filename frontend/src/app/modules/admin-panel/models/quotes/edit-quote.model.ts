export class EditQuote {
  quoteId: number;
  quote: string;
  context: string;
}
