export class NewQuote {
  characterId: number;
  quote: string;
  context: string;
}
