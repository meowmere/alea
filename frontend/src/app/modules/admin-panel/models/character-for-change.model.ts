export class CharacterForChange {
  constructor(id, archived) {
    this.id = id
    this.archived = archived;
  }
  id: number;
  archived: boolean;

}
