export interface IProfilePic {
  image: string;
  extension: string;
}
