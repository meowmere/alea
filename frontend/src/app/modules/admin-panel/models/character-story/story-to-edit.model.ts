export class EditStory {
  storyId: number;
  title: string;
  desc: string;
}
