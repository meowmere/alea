export class EditRelationship {
  characterId: number;
  relatedCharacterId: number;
  relationType: string | null;
  reversedRelationType: string | null;
}
