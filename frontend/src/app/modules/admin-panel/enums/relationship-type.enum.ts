// W przypadku dodania nowego typu, trzeba tak samo zaktualizować enum na BE!

export enum RelationshipType {
  PARENT,
  CHILD,
  MARRIAGE
}
