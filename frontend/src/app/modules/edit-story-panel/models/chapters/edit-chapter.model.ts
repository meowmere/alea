export class EditChapter {
  id: number | null;
  name: string;
  chapterDesc: string;
  bookId: number;
}
