import { AvailableIcons } from './../../enums/AvailableIcons.enum';
export class EditBook {
  id: number;
  icon: AvailableIcons | null;
  name: string;
  color: string;
}
