export interface INationalitiesStatistics {
  nationality: string;
  num: number;
}

export class NationalitiesStatistics implements INationalitiesStatistics {
  nationality: string;
  num: number;
}
