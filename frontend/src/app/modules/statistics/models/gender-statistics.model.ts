export interface IGenderStatistics {
  maleNumber: number;
  femaleNumber: number;
}
