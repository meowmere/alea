export interface IAgeStat {
  label: string;
  count: number;
  details: Map<number, number>
}
