export interface ITypeStatistics {
  mainCharactersNum: number;
  sideCharactersNum: number;
  bgCharactersNum: number;
}
